﻿namespace lib
{
    public abstract class EPaperBase
    {
        protected readonly IEPaperConnection _ePaperConnection;
        protected readonly Connection _connection;
        
        public EPaperBase()
        {
            _connection = new Connection(new HardwareSpecification());
            _ePaperConnection = new EPaperConnection(_connection);
        }        
        
        public abstract void Initialize();
        public abstract void Reset();
        public abstract void WaitUntilIdle();
        public abstract void ClearScreen();
        public abstract void Sleep();
    }
}