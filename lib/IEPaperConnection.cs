﻿namespace lib
{
    public interface IEPaperConnection
    {
        void SendCommand(byte command);
        void SendData(byte data);
        void SendDataStream(byte[] dataStream);
        void Send(Command cmd);
    }
}