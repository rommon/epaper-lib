﻿using System.Collections.Generic;
using System.Linq;

namespace lib
{
    public struct Command
    {
        public byte Cmd;
        public byte[] Data;

        public Command(byte cmd, byte[] data = null)
        {
            this.Cmd = cmd;
            
            data ??= new byte[] { };

            this.Data = data;
        }

        public static byte[] GetDataArrayWithValue(int width, int height, byte value)
        {
            var list = new List<byte>();
            foreach (var i in Enumerable.Range(0, width * height))
            {
                list.Add(value);
            }

            return list.ToArray();
        }
    }
}