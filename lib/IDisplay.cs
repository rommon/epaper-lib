﻿namespace lib
{
    public interface IDisplay
    {
        public void Reset();
        public void SendCommand(byte command);
        public void SendData(byte data);
        public void ReadBusy();
        public void SetLut();
        public void GraySetLut();
    }
}