﻿using System;
using System.Threading;
using Swan;

namespace lib
{
    public class EPaper27 : EPaperBase
    {
        public EPaper27() : base()
        {
        }

        public override void Initialize()
        {
            _connection.Initialize();
            Reset();

            _ePaperConnection.Send(HardwareCodes.PowerSettings);
            _ePaperConnection.Send(HardwareCodes.BoosterSoftStart);
            _ePaperConnection.Send(HardwareCodes.PowerOptimization1);
            _ePaperConnection.Send(HardwareCodes.PowerOptimization2);
            _ePaperConnection.Send(HardwareCodes.PowerOptimization3);
            _ePaperConnection.Send(HardwareCodes.PowerOptimization4);
            _ePaperConnection.Send(HardwareCodes.PowerOptimization5);
            _ePaperConnection.Send(HardwareCodes.PowerOptimization6);
            _ePaperConnection.Send(HardwareCodes.PowerOptimization7);
            _ePaperConnection.Send(HardwareCodes.PartialDisplayRefresh);
            _ePaperConnection.Send(HardwareCodes.PowerOn);
            WaitUntilIdle();

            _ePaperConnection.Send(HardwareCodes.PanelSettings);
            _ePaperConnection.Send(HardwareCodes.PllControl);
            _ePaperConnection.Send(HardwareCodes.VcmDcSettingRegister);

            SetLut();
        }

        private void SetLut()
        {
            _ePaperConnection.Send(HardwareCodes.LutVcomDc);
            _ePaperConnection.Send(HardwareCodes.LutWw);
            _ePaperConnection.Send(HardwareCodes.LutBw);
            _ePaperConnection.Send(HardwareCodes.LutBb);
            _ePaperConnection.Send(HardwareCodes.LutWb);
        }

        public override void WaitUntilIdle()
        {
            Console.WriteLine("e-Paper busy");
            while (_connection.BusyPin.Read() == false)
            {
                Thread.Sleep(200);
            }

            Console.WriteLine("e-Paper busy release");
        }

        public override void Reset()
        {
            _connection.ResetPin.Write(true);
            Thread.Sleep(200);
            _connection.ResetPin.Write(false);
            Thread.Sleep(10);
            _connection.ResetPin.Write(true);
            Thread.Sleep(200);
        }

        public override void ClearScreen()
        {
            _ePaperConnection.Send(HardwareCodes.Clear1);
            _ePaperConnection.Send(new Command(HardwareCodes.CmdDataStop));
            _ePaperConnection.Send(HardwareCodes.Clear2);
            _ePaperConnection.Send(HardwareCodes.Clear3);
            WaitUntilIdle();
        }

        public override void Sleep()
        {
            _ePaperConnection.Send(HardwareCodes.Sleep1);
            _ePaperConnection.Send(HardwareCodes.Sleep2);
            _ePaperConnection.Send(HardwareCodes.Sleep3);
        }

        public void Display(byte[] image)
        {
            _ePaperConnection.Send(HardwareCodes.Clear1);
            _ePaperConnection.Send(new Command(HardwareCodes.CmdDataStartTransmission2, image));
            _ePaperConnection.Send(new Command(HardwareCodes.CmdDataStop));
            _ePaperConnection.SendCommand(HardwareCodes.CmdDisplayRefresh);
            WaitUntilIdle();
        }
    }
}