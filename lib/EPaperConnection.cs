﻿using Unosquare.RaspberryIO.Abstractions;

namespace lib
{
    public class EPaperConnection : IEPaperConnection
    {
        private readonly Connection _connection;
        private static readonly object _syncLock = new object();

        public EPaperConnection(Connection connection)
        {
            _connection = connection;
        }

        public void SendCommand(byte command)
        {
            lock (_syncLock)
            {
                _connection.DcPin.Write(GpioPinValue.Low);
                _connection.CsPin.Write(GpioPinValue.Low);
                _connection.Channel.SendReceive(new byte[]{command});
                _connection.CsPin.Write(GpioPinValue.High);
            }
        }

        public void SendData(byte data)
        {
            lock (_syncLock)
            {
                _connection.DcPin.Write(GpioPinValue.High);
                _connection.CsPin.Write(GpioPinValue.Low);
                _connection.Channel.SendReceive(new[] {data});
                _connection.CsPin.Write(GpioPinValue.High);
            }
        }

        public void SendDataStream(byte[] dataStream)
        {
            foreach (var b in dataStream)
            {
                SendData(b);
            }
        }

        public void Send(Command cmd)
        {
            SendCommand(cmd.Cmd);
            SendDataStream(cmd.Data);
        }
    }
}