﻿namespace lib
{
    public class HardwareSpecification : IHardwareSpecification
    {
        public int RST_PIN => 17;

        public int DC_PIN => 25;

        public int CS_PIN => 8;

        public int BUSY_PIN => 24;

        public int Channel0Frequency => 4000000;
    }
}