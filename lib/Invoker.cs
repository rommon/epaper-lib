﻿using System;
using System.Collections.Generic;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Drawing.Processing;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using Unosquare.RaspberryIO;
using Unosquare.WiringPi;

namespace lib
{
    public class Invoker
    {
        static void Main(string[] args)
        {
            Pi.Init<BootstrapWiringPi>();

            var epaper = new EPaper27();
            epaper.Initialize();

            // var file = new FileStream("2in7_stripped.bmp", FileMode.Open, FileAccess.Read);
            // var img = Image.Load<L8>("2in7.bmp");
            // img.Mutate(i => i.BlackWhite().Rotate(90).Flip(FlipMode.Horizontal));
            var img = Image.LoadPixelData<L8>(
                Command.GetDataArrayWithValue(Epd2In7Parameter.Height, Epd2In7Parameter.Width, 0xFF),
                Epd2In7Parameter.Height, Epd2In7Parameter.Width);
            FontCollection collection = new FontCollection();
            IEnumerable<FontFamily> families = collection.InstallCollection("Font.ttc");
            var family = collection.Find("WenQuanYi Micro Hei");
            Font font = family.CreateFont(30, FontStyle.Regular);
            ///usr/share/fonts/truetype/liberation2/LiberationMono-Regular.ttf
            string yourText = "Abcdefg";

            img.Mutate(x => x.DrawText(yourText, font, Color.Black, new PointF(10, 10)));
            img.Mutate(x => x.DrawLines(Color.Black, 5.0f, new PointF(0, 0), new PointF(0, 100)));

            img.Mutate(i => i.BlackWhite());
            var data = GetRawData(img);
            // var data2 = new byte[] { 0x00,0x00,0x00,0x00,0xff,0xff,0xff,0xff,0x00,0x00,0x00,0x00,0xff,0xff,0xff,0xff,0x00,0x00,0x00,0x00,0xff,0xff,0xff,0xff};
            // var data = new byte[] {0x7f}; // ~0b10000000
            // epaper.ClearScreen();
            epaper.Display(data);
            Console.WriteLine("exiting...");
        }

        public static byte[] GetRawData(Image<L8> image)
        {
            image.Mutate(i => i.Flip(FlipMode.Horizontal));
            var data = new List<byte>();

            for (var x = 0; x < image.Width; x++)
            {
                for (var y = 0; y < image.Height; y += 8)
                {
                    var value128 = image[x, y + 7].PackedValue;
                    var value64 = image[x, y + 6].PackedValue;
                    var value32 = image[x, y + 5].PackedValue;
                    var value16 = image[x, y + 4].PackedValue;
                    var value8 = image[x, y + 3].PackedValue;
                    var value4 = image[x, y + 2].PackedValue;
                    var value2 = image[x, y + 1].PackedValue;
                    var value1 = image[x, y].PackedValue;


                    var b128 = (byte) (0b00000001 & value128);
                    var b64 = (byte) (0b00000010 & value64);
                    var b32 = (byte) (0b00000100 & value32);
                    var b16 = (byte) (0b00001000 & value16);
                    var b8 = (byte) (0b00010000 & value8);
                    var b4 = (byte) (0b00100000 & value4);
                    var b2 = (byte) (0b01000000 & value2);
                    var b1 = (byte) (0b10000000 & value1);

                    var b = (byte) (b1 | b2 | b4 | b8 | b16 | b32 | b64 | b128);

                    data.Add(b);
                    Console.Write(Convert.ToString(b, 2).PadLeft(8, '0'));
                }

                Console.WriteLine();
            }

            return data.ToArray();
        }
    }
}