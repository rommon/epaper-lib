﻿namespace lib
{
    public static class Epd2In7Parameter
    {
        public static readonly int Width = 176;
        public static readonly int Height = 264;

        public static readonly byte Gray1 = 0xff; // white
        public static readonly byte Gray2 = 0xc0;
        public static readonly byte Gray3 = 0x80; // gray
        public static readonly byte Gray4 = 0x00; // darkest

        
    }
}